% TicTacToes Simulator

% List utils
% -------------------------------------------------------------
% replace(?ElemToReplace, ?NewElem, ?List, ?NewList)
replace(_, _, [], []).
replace(O, R, [O|T], [R|T2]) :- replace(O, R, T, T2),!.
replace(O, R, [H|T], [H|T2]) :- H \= O, replace(O, R, T, T2).
% -------------------------------------------------------------


% Cells sequence check predicates
% -------------------------------------------------------------

% aligned(?Table, ?AlignedSequence)
% row sequences
aligned([C1,C2,C3,C4,C5,C6,C7,C8,C9], [C1,C2,C3]).
aligned([C1,C2,C3,C4,C5,C6,C7,C8,C9], [C4,C5,C6]).
aligned([C1,C2,C3,C4,C5,C6,C7,C8,C9], [C7,C8,C9]).
% column sequences
aligned([C1,C2,C3,C4,C5,C6,C7,C8,C9], [C1,C4,C7]).
aligned([C1,C2,C3,C4,C5,C6,C7,C8,C9], [C2,C5,C8]).
aligned([C1,C2,C3,C4,C5,C6,C7,C8,C9], [C3,C6,C9]).
% diagonal/anti-diagonal sequences
aligned([C1,C2,C3,C4,C5,C6,C7,C8,C9], [C1,C5,C9]).
aligned([C1,C2,C3,C4,C5,C6,C7,C8,C9], [C3,C5,C7]).


% wincombination(+Cell1, +Cell2, +Cell3)
wincombination(cell(X1,Y1,S), cell(X2,Y2,S), cell(X3,Y3,S)) :- ((((X2 is X1 + 1, X3 is X2 + 1);(X2 is X1 - 1, X3 is X2 - 1)), Y2 is Y1, Y3 is Y2); % row win
																															            (((Y2 is Y1 + 1, Y3 is Y2 + 1);(Y2 is Y1 - 1, Y3 is Y2 - 1)), X2 is X1, X3 is X2); % column win
																															            (X2 is X1 + 1, X3 is X2 + 1, Y2 is Y1 + 1, Y3 is Y2 + 1);	   % diagonal win 
																															            (X2 is X1 - 1, X3 is X2 - 1, Y2 is Y1 - 1, Y3 is Y2 - 1)),!, % anti-diagonal win
																						                              X1 =< 3, X2 =< 3, X3 =< 3, Y1 =< 3, Y2 =< 3, Y3 =< 3,   % upper boundaries costraints
																																          X1 >= 1, X2 >= 1, X3 >= 1, Y1 >= 1, Y2 >= 1, Y3 >= 1.  	% lower boundaries costraints						  	

% selectwinningcell(+Cell1, +Cell2, +Cell3, +PlayerSign, -OldCell, -NewCell)
selectwinningcell(cell(X1,Y1,S), cell(X2,Y2,NS), cell(X3,Y3,S), S, cell(X2,Y2,NS), cell(X2,Y2,S)) :- NS = nothing,
																																																	wincombination(cell(X1,Y1,S), cell(X2,Y2,S), cell(X3,Y3,S)),!.
selectwinningcell(cell(X1,Y1,NS), cell(X2,Y2,S), cell(X3,Y3,S), S, cell(X1,Y1,NS), cell(X1,Y1,S)) :- NS = nothing,
																																																	wincombination(cell(X1,Y1,S), cell(X2,Y2,S), cell(X3,Y3,S)),!.
selectwinningcell(cell(X1,Y1,S), cell(X2,Y2,S), cell(X3,Y3,NS), S, cell(X3,Y3,NS), cell(X3,Y3,S)) :- NS = nothing,
																																																	wincombination(cell(X1,Y1,S), cell(X2,Y2,S), cell(X3,Y3,S)).


% checkwinningsequence(+Cell1, +Cell2, +Cell3, +PlayerSign)
checkwinningsequence(cell(X1,Y1,S), cell(X2,Y2,S), cell(X3,Y3,S), S) :- wincombination(cell(X1,Y1,S), cell(X2,Y2,S), cell(X3,Y3,S)),!.
checkwinningsequence(cell(X1,Y1,S), cell(X2,Y2,S), cell(X3,Y3,S), S) :- wincombination(cell(X1,Y1,S), cell(X2,Y2,S), cell(X3,Y3,S)),!.
checkwinningsequence(cell(X1,Y1,S), cell(X2,Y2,S), cell(X3,Y3,S), S) :- wincombination(cell(X1,Y1,S), cell(X2,Y2,S), cell(X3,Y3,S)).

% selectbestcell(@Table, -X, -Y)
%	It could be improved with more strategies...														 
selectbestcell(Table, 2, 2) :- member(cell(2,2, nothing), Table).  % move in central cell if empty
selectbestcell(Table, X, Y) :- member(cell(X,Y, nothing), Table).  % any empty cell

% -------------------------------------------------------------

% Game utils
% -------------------------------------------------------------
%opponent(?Player, ?OpponentPlayer)
opponent(x, o).
opponent(o, x).


% fulltable(+Table)
fulltable([]).
fulltable([cell(X,Y,S)|T]) :- S \= nothing,
															fulltable(T).
% -------------------------------------------------------------



% next(@Table,@Player,-Result,-NewTable)
next(Table, Player, win, NewTable) :-	aligned(Table, [C1, C2, C3]), 												% winning case
																					selectwinningcell(C1, C2, C3, Player, OldCell, NewCell),										
																					replace(OldCell, NewCell, Table, NewTable),!.

next(Table, Player, lose, NewTable) :- aligned(Table, [C1, C2, C3]), 												% losing case
																			 		 opponent(Player, Opponent),
																			 		 selectwinningcell(C1, C2, C3, Opponent, OldCell, NewCell),
																			 		 replace(OldCell, NewCell, Table, NewTable),!.

next(Table, Player, even, Table) :- aligned(Table, [C1, C2, C3]), 													% even case
																				opponent(Player, Opponent),
																				not selectwinningcell(C1, C2, C3, Opponent, _, _),
																				not selectwinningcell(C1, C2, C3, Player, _, _),
																				fulltable(Table),!.

next(Table, Player, nothing, NewTable) :- aligned(Table, [C1, C2, C3]), 										% nothing case
																			 		 opponent(Player, Opponent),
																			 		 not selectwinningcell(C1, C2, C3, Opponent, _, _),
																			 		 not selectwinningcell(C1, C2, C3, Player, _, _),
																			 		 selectbestcell(Table, X, Y),
																			 		 replace(cell(X,Y,nothing), cell(X,Y,Player), Table, NewTable),!.


																	

% game(@Table,@Player,-Result,-TableList)
% Full table
game(T, P, R, TL) :- game_(T,P,P,R,TL),
										 not fulltable(T).
game(T, P, win, T) :- fulltable(T),
									    aligned(T, [C1,C2,C3]),
									    checkwinningsequence(C1,C2,C3,P),!.
game(T, P, lose, T) :- fulltable(T),
									     aligned(T, [C1,C2,C3]),
									     opponent(P, O),
									     checkwinningsequence(C1,C2,C3,O),!.
game(T, P, even, T) :- fulltable(T).

% game_(+Table, +Player, +Turn, -Result, - TableList)
% Used if players can still move
game_(Table, P, P, win, TL) :- next(Table, P, win, TL),!.
game_(Table, P, P, lose, TL) :- next(Table, P, lose, TL),!.	
game_(Table, P, P, even, TL) :- next(Table, P, even, TL),!.
game_(Table, P, Turn, R, TL) :- next(Table, Turn, nothing, NT),						        		
										 		    		  opponent(Turn, OP),
										 		    		  game_(NT, P, OP, R, TL).



% Output utils
% -------------------------------------------------------------

% displaytable(+Table)
displaytable([cell(_,_,S1),cell(_,_,S2),cell(_,_,S3)|T]) :-   write([S1,S2,S3]), nl,
																														  displaytable(T).


% printnext(+Table,+Player)
printnext(T, P) :- next(T, P, R, NT),
													write(R),nl,
										 			displaytable(NT).

% printgame(+Table,+Player)
printgame(T,P):-game(T,P,R,TL), 
								write(R),nl,
								displaytable(TL).
% -------------------------------------------------------------
