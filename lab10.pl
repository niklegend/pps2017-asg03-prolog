%search(Elem,List)
search(X,[X|_]).
search(X,[_|Xs]):-search(X,Xs).

%search2(Elem,List)
%looks for two consecutive occurrences of Elem
search2(X,[X,X|_]).
search2(X,[_|Xs]) :- search2(X,Xs).

% search_two(Elem,List)
% looks for two occurrences of Elem with an element in between!
search_two(X,[X|[Y,X|_]]).
search_two(X,[_|Xs]) :- search_two(X, Xs).

% search_anytwo(Elem,List)
% looks for any Elem that occurs two times
search_anytwo(E, [E|T]) :- search(E,T).
search_anytwo(E,[_|T]) :- search_anytwo(E,T).

% size(List,Size)
% Size will contain the number of elements in List
% size([],0).
% size([_|T],M) :- size(T,N), M is N+1.

% size(List,Size)
% Size will contain the number of elements in List, written using notation zero, s(zero), s(s(zero))..
size([], zero).
size([_|T], s(M)) :- size(T, M).

% sum(List,Sum)
sum([], 0).
sum([H|T], M) :- sum(T,N), M is N+H.

% average(List,Average)
% it uses average(List,Count,Sum,Average)
average(List,A) :- average(List,0,0,A).
average([],C,S,A) :- A is S/C.
average([X|Xs],C,S,A) :- C2 is C+1,
												 S2 is S+X,
												 average(Xs,C2,S2,A).

% max(List,Max)
% Max is the biggest element in List
% Suppose the list has at least one element
max([H|T],Max) :- max(T,Max,H).
max([], Max, Max).
max([X|Xs], Max, TempMax) :- X =< TempMax,
														 max(Xs, Max, TempMax).
max([X|Xs], Max, TempMax) :- X > TempMax,
														 max(Xs, Max, X).

% same(List1,List2)
% are the two lists the same?
same([],[]).
same([X|Xs],[X|Ys]):- same(Xs,Ys).


% all_bigger(List1,List2)
% all elements in List1 are bigger than those in List2, 1 by 1
% example: all_bigger([10,20,30,40],[9,19,29,39]).
all_bigger([X], [Y]) :- X > Y.
all_bigger([X|Xs], [Y|Ys]) :- X > Y,
															all_bigger(Xs, Ys).


% sublist(List1,List2)
% List1 should be a subset of List2
% example: sublist([1,2],[5,3,2,1]).
sublist([],[Y|Ys]).
sublist([X|Xs], [Y|Ys]) :- search(X, [Y|Ys]), 
													 sublist(Xs, Ys).


% seq(N,List)
% example: seq(5,[0,0,0,0,0]).
seq(0,[]).
seq(N,[0|T]):- N > 0, N2 is N-1, seq(N2,T).


% seqR(N,List)
% example: seqR(4,[4,3,2,1,0]).
seqR(0,[0]).
seqR(N,[N|T]) :- N > 0,
								 N2 is N-1,
								 seqR(N2,T).


% last(List, elem)
% example last([0,1,2,3],3).
last([X], X).
last([X|Xs], Y) :- last(Xs, Y).


% seqR2(N,List)
% example: seqR2(4,[0,1,2,3,4]).
seqR2(N, [H|T]) :- reverse([H|T], R),
									 seqR(N, R).


% inv(List,List)
% example: inv([1,2,3],[3,2,1]).
inv([],[]).
inv([A], [A]) :- !.
inv([H|T], RH) :- append(R, [H], RH),
							   	inv(T, R).


% invTail(List, List)
% More efficient version of inv(List, List) -> O(N)
invTail(Xs,Ys) :-            % to reverse a list of any length, simply invoke the
  invTail_(Xs,[],Ys) . % invTail_ predicate with the accumulator seeded as the empty list

invTail_([], R, R).    % if the list is empty, the accumulator contains the reversed list
invTail_([X|Xs], T, R) :-  % if the list is non-empty, we reverse the list
  invTail_(Xs, [X|T], R).      % by recursing down with the head of the list prepended to the accumulator


% double(List,List)
% suggestion: remember predicate append/3
% example: double([1,2,3],[1,2,3,1,2,3]).
double([],[]).
double(L, LD) :- append(L, L, LD).

% times(List,N,List)
% example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).
times([], N, []):- !.
times(L, 0, []) :- !.
times(L, N, LT) :-  N2 is N - 1,
										append(L, Acc, LT),
										times(L, N2, Acc).


% proj(List,List)
% example: proj([[1,2],[3,4],[5,6]],[1,3,5]).
proj([], []).
proj([[H|_]|T2], [H|X]):- proj(T2, X).
