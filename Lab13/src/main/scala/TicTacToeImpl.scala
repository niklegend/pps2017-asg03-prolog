import java.io.FileInputStream

import Scala2P._
import alice.tuprolog.Theory


class TicTacToeImpl(fileName: String) extends TicTacToe {

  private val engine = mkPrologEngine(new Theory(new FileInputStream(fileName)))

  createBoard()

  override def createBoard(): Unit = {
    val goal = "retractall(board(_)),newboard(B),assert(board(B))"
    solveWithSuccess(engine, goal)
  }

  override def checkCompleted(): Boolean =
    solveWithSuccess(engine, "board(B),boardfilled(B)")


  override def checkVictory(): Boolean =
    solveWithSuccess(engine, "board(B),threeinarow(B)")


  override def isAFreeCell(i: Int, j: Int): Boolean =
    solveWithSuccess(engine, s"board(B),not(filledsquare(B,${i * 3 + j + 1}))")

  private def setCell(pos: Int, player: String): Unit = {
    val goal = s"retract(board(B)),!,setsquare(B,$pos,$player,B2),assert(board(B2))"
    solveWithSuccess(engine, goal)
  }

  override def setHumanCell(i: Int, j: Int): Unit = {
    setCell(i * 3 + j + 1, "'X'")
  }

  override def setComputerCell(): Array[Int] = {
    // a solution which seeks for the best cell to move
    val pos = (solveOneAndGetTerm(engine, "board(B), response(B, 'O', X)", "X") toString()).toInt
    println("Best cell to move is: " + pos)
    setCell(pos, "'O'")
    Array((pos - 1) / 3, (pos - 1) % 3)
  }

  override def toString: String =
    solveOneAndGetTerm(engine, "board(B)", "B").toString

}
