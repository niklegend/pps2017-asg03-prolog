% dropAny(?Elem,?List,?OutList)
dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).

% dropFirst(?Elem,?List,?OutList)
dropFirst(X,[X|T],T):-!.
dropFirst(X,[H|Xs],[H|L]):-dropFirst(X,Xs,L).

% dropLast(?Elem,?List,?OutList)
dropLast(X, L, OL):- reverse(L, RX),
										 dropFirst(X, RX, OLR),
										 reverse(OLR, OL).

% dropAll(?Elem,?List,?OutList)
dropAll(X,[],[]).
dropAll(X, [X|T], R):- !,dropAll(X, T, R).
dropAll(X, [H|T], [H|R]):- H \= X,
													 dropAll(X, T, R).
						
% fromList(+List,-Graph)
fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]):- fromList([H2|T],L).

% fromCircList(+List,-Graph)
fromCircList([H|T], G):- fromCircList(H, [H|T], G).
fromCircList(FirstH, [_], []).
fromCircList(FirstH, [X], [e(X,FirstH)]).
fromCircList(FirstH, [H1,H2|T], [e(H1,H2)|L]):- fromCircList(FirstH, [H2|T], L).

% dropNode(+Graph, +Node, -OutGraph)
% drop all edges starting and leaving from a Node
% use dropAll defined in 1.1
dropNode(G,N,O):- dropAll(G,e(N,_),G2),
								  dropAll(G2,e(_,N),O).

% reaching(+Graph, +Node, -List)
% all the nodes that can be reached in 1 step from Node
reaching(L, Node, OL):- reaching(L, L, Node, OL).
reaching(OriginL,[], Node, []):- member(e(Node,_), OriginL),!.
reaching(OriginL,[e(X,_)|T], Node, OL) :- 	X \= Node,
															    					reaching(OriginL,T, Node, OL).
reaching(OriginL,[e(Node,N2)|T], Node, [N2|T2]) :- reaching(OriginL, T, Node, T2).


% anypath(+Graph, +Node1, +Node2, -ListPath)
% a path from Node1 to Node2
% if there are many path, they are showed 1-by-1
anypath(G, Node1, Node2, [e(Node1,Node2)]):- member(e(Node1,Node2), G).
anypath(G, Node1, Node2, [e(Node1,Node3)|T]):- member(e(Node1,Node3), G),
																							 anypath(G, Node3, Node2, T).


% allreaching(+Graph, +Node, -List)
% all the nodes that can be reached from Node
% Suppose the graph is NOT circular!
% Use findall and anyPath!

% remove_dups(+List, -NewList): 
% NewList is bound to List, but with duplicate items removed.
remove_dups([], []).
remove_dups([First | Rest], NewRest) :- member(First, Rest),
																				remove_dups(Rest, NewRest),!.
remove_dups([First | Rest], [First | NewRest]) :- not(member(First, Rest)),
																									remove_dups(Rest, NewRest).
																					  
allreaching(G, Node, OL):- member(e(Node,_), G),!,
													 findall(X, anypath(G, Node, X, _), L),
													 remove_dups(L, OL).